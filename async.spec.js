import { EventEmitter } from 'events'
import { Readable, PassThrough } from 'stream'
import {
  Async,
  empty,
  enumerate,
  from,
  fromPromise,
  fromIterable,
  fromEvent,
  toArray,
  map,
  filter,
  scan,
  reduce,
  flatten,
  take,
  skip,
  takeUntil,
  skipUntil,
  concat,
  zip,
  copy,
  intercalate,
  some,
  every,
  automata,
  tap,
  forEach
} from './async.js'

import {
  describe,
  xdescribe,
  it,
  xit,
  expect,
  mock,
  Async as AsyncTest
} from 'polypore'

const tests = describe('Hyphae', async function* () {
  yield* describe('Async Stream', async function* () {
    yield* describe('fromIterable', async function* () {
      yield* it('should convert an array into a sync stream', async function* () {
        const stream = fromIterable(['worf', 'borf', 'snarf'])
        yield expect(await stream.next()).to.deepEqual({
          done: false,
          value: 'worf'
        })
        yield expect(await stream.next()).to.deepEqual({
          done: false,
          value: 'borf'
        })
        yield expect(await stream.next()).to.deepEqual({
          done: false,
          value: 'snarf'
        })
        yield expect(await stream.next()).to.deepEqual({
          done: true,
          value: undefined
        })

        const streamObj = Async.fromIterable(['worf', 'borf', 'snarf'])
        yield expect(await streamObj.stream.next()).to.deepEqual({
          done: false,
          value: 'worf'
        })
        yield expect(await streamObj.stream.next()).to.deepEqual({
          done: false,
          value: 'borf'
        })
        yield expect(await streamObj.stream.next()).to.deepEqual({
          done: false,
          value: 'snarf'
        })
        yield expect(await streamObj.stream.next()).to.deepEqual({
          done: true,
          value: undefined
        })
      })
    })

    yield* describe('from', async function* () {
      yield* it('should convert a value into a stream', async function* () {
        yield expect(await toArray(from('hello'))).to.deepEqual(['hello'])
        yield expect(await Async.from('hello').toArray()).to.deepEqual([
          'hello'
        ])
      })
    })

    yield* describe('fromPromise', async function* () {
      yield* it('should convert a promise into a stream', async function* () {
        yield expect(
          await toArray(fromPromise(Promise.resolve('hello')))
        ).to.deepEqual(['hello'])
        yield expect(
          await Async.fromPromise(Promise.resolve('hello')).toArray()
        ).to.deepEqual(['hello'])
      })
    })

    yield* describe('fromEvent', async function* () {
      yield* it('should create a stream from an event', async function* () {
        const emitter = new EventEmitter()

        const stream = fromEvent(emitter, 'myEvent')
        yield expect(stream.unsubscribe).to.beOk

        const result = toArray(stream)

        await new Promise(resolve => {
          setTimeout(() => {
            emitter.emit('myEvent', 1)
            emitter.emit('myEvent', 2)
            emitter.emit('myEvent', 3)

            stream.unsubscribe()
            resolve()
          }, 100)
        })

        yield expect(await result).to.deepEqual([1, 2, 3])
      })

      yield* it('should create an object stream from an event', async function* () {
        const emitter = new EventEmitter()

        const streamObj = Async.fromEvent(emitter, 'myEvent')
        yield expect(streamObj.unsubscribe).to.beOk

        const result = streamObj.toArray()

        await new Promise(resolve => {
          setTimeout(() => {
            emitter.emit('myEvent', 1)
            emitter.emit('myEvent', 2)
            emitter.emit('myEvent', 3)

            streamObj.unsubscribe()
            resolve()
          }, 100)
        })

        yield expect(await result).to.deepEqual([1, 2, 3])
      })
    })

    yield* describe('fromReadableStream', async function* () {
      yield* it('should create an async stream from a nodejs readable stream', async function* () {
        const readableStream = new Readable()
        readableStream.push('foo')
        const stream = Async.fromReadable(readableStream)
        readableStream.push('bar')
        readableStream.push('baz')
        const result = stream.toArray()
        readableStream.push(null)
        yield expect(await result).to.deepEqual([
          Buffer.from('foo'),
          Buffer.from('bar'),
          Buffer.from('baz')
        ])
      })
    })

    yield* describe('toWritableStream', async function* () {
      yield* it('should output the stream to a nodejs writeable stream', async function* () {
        const stream = new PassThrough()
        await Async.fromIterable(['foo', 'bar', 'baz']).toWritable(stream)
        yield expect(stream.read()).to.deepEqual(Buffer.from('foobarbaz'))
      })
    })

    yield* describe('fromTimeout', async function* () {
      yield* it('should convert a timeout into an async stream', async function* () {
        yield expect(await Async.fromTimeout('foo').toArray()).to.deepEqual([
          'foo'
        ])
      })
    })

    yield* describe('fromInterval', async function* () {
      yield* it('should create an interval counter as an async stream', async function* () {
        yield expect(
          await Async.fromInterval(20).take(4).toArray()
        ).to.deepEqual([0, 1, 2, 3])
      })
    })

    yield* describe('toArray', async function* () {
      yield* it('should convert an async stream into an array', async function* () {
        const array = ['worf', 'borf', 'snarf']
        yield expect(await toArray(fromIterable(array))).to.deepEqual(array)
        yield expect(await Async.fromIterable(array).toArray()).to.deepEqual(
          array
        )
      })
    })

    yield* describe('empty', async function* () {
      yield* it('should return an empty stream', async function* () {
        yield expect(await toArray(empty())).to.deepEqual([])
        yield expect(await Async.empty().toArray()).to.deepEqual([])
      })
    })

    yield* describe('enumerate', async function* () {
      yield* it('should enumerate n times', async function* () {
        yield expect(await toArray(enumerate(5))).to.deepEqual([1, 2, 3, 4, 5])
        yield expect(await Async.enumerate(5).toArray()).to.deepEqual([
          1,
          2,
          3,
          4,
          5
        ])
      })
    })

    yield* describe('map', async function* () {
      const fn = s => `{s}!`

      yield* it('should map over the stream', async function* () {
        const array = ['mirf', 'truff', 'quif']
        yield expect(await toArray(map(fromIterable(array), fn))).to.deepEqual(
          array.map(fn)
        )
        yield expect(
          await Async.fromIterable(array).map(fn).toArray()
        ).to.deepEqual(array.map(fn))
      })

      yield* it('should map over an empty stream', async function* () {
        yield expect(await toArray(map(empty(), fn))).to.deepEqual([])
        yield expect(await Async.empty().map(fn).toArray()).to.deepEqual([])
      })
    })

    yield* describe('filter', async function* () {
      const even = e => e % 2 === 0

      yield* it('should filter the stream', async function* () {
        yield expect(await toArray(filter(enumerate(7), even))).to.deepEqual([
          2,
          4,
          6
        ])
        yield expect(
          await Async.enumerate(7).filter(even).toArray()
        ).to.deepEqual([2, 4, 6])
      })

      yield* it('should filter an empty stream', async function* () {
        yield expect(await toArray(filter(empty(), even))).to.deepEqual([])
        yield expect(await Async.empty().filter(even).toArray()).to.deepEqual(
          []
        )
      })
    })

    yield* describe('transducer', async function* () {
      yield* it('should be have like a transducer', async function* () {
        let events = []

        await Async.enumerate(7)
          .map(e => {
            const result = e + 1
            events.push(`map: ${e} => ${result}`)
            return result
          })
          .filter(e => {
            const isFiltered = e % 2 === 0
            events.push(`filter: ${e} (${isFiltered})`)
            return isFiltered
          })
          .toArray()

        yield expect(events).to.deepEqual([
          'map: 1 => 2',
          'filter: 2 (true)',
          'map: 2 => 3',
          'filter: 3 (false)',
          'map: 3 => 4',
          'filter: 4 (true)',
          'map: 4 => 5',
          'filter: 5 (false)',
          'map: 5 => 6',
          'filter: 6 (true)',
          'map: 6 => 7',
          'filter: 7 (false)',
          'map: 7 => 8',
          'filter: 8 (true)'
        ])
      })
    })

    yield* describe('scan', async function* () {
      const sum = (acc, e) => acc + e

      yield* it('should scan the stream', async function* () {
        yield expect(await toArray(scan(enumerate(5), sum, 0))).to.deepEqual([
          1,
          3,
          6,
          10,
          15
        ])
        yield expect(
          await Async.enumerate(5).scan(sum, 0).toArray()
        ).to.deepEqual([1, 3, 6, 10, 15])
      })

      yield* it('should scan the empty stream', async function* () {
        yield expect(await toArray(scan(empty(), sum, 0))).to.deepEqual([])
        yield expect(await Async.empty().scan(sum, 0).toArray()).to.deepEqual(
          []
        )
      })
    })

    yield* describe('reduce', async function* () {
      const sum = (acc, e) => acc + e

      yield* it('should reduce the stream', async function* () {
        yield expect(await reduce(enumerate(5), sum, 0)).to.equal(15)
        yield expect(await Async.enumerate(5).reduce(sum, 0)).to.equal(15)
      })

      yield* it('should reduce the empty stream', async function* () {
        yield expect(await reduce(empty(), sum, 0)).to.equal(0)
        yield expect(await Async.empty().reduce(sum, 0)).to.equal(0)
      })
    })

    yield* describe('flatten', async function* () {
      yield* it('should flatten the stream', async function* () {
        yield expect(
          await Async.fromIterable('hi')
            .map(
              async char =>
                await Async.fromIterable([10, 20, 30]).map(num => char + num)
            )
            .flatten()
            .toArray()
        ).to.deepEqual(['h10', 'h20', 'h30', 'i10', 'i20', 'i30'])
      })
    })

    yield* describe('take', async function* () {
      yield* it('should take n elements from the stream', async function* () {
        yield expect(await toArray(take(enumerate(8), 5))).to.deepEqual([
          1,
          2,
          3,
          4,
          5
        ])
        yield expect(await Async.enumerate(8).take(5).toArray()).to.deepEqual([
          1,
          2,
          3,
          4,
          5
        ])
      })

      yield* it('should handle an empty array', async function* () {
        yield expect(await toArray(take(empty(), 5))).to.deepEqual([])
        yield expect(await Async.empty().take(5).toArray()).to.deepEqual([])
      })

      yield* it('should handle an n greater than the number of elements in the stream', async function* () {
        const array = [1, 2, 3, 4]
        yield expect(await toArray(take(fromIterable(array), 5))).to.deepEqual(
          array
        )
        yield expect(
          await Async.fromIterable(array).take(5).toArray()
        ).to.deepEqual(array)
      })

      yield* it('should limit an infinite series', async function* () {
        yield expect(await toArray(take(enumerate(), 5))).to.deepEqual([
          1,
          2,
          3,
          4,
          5
        ])
        yield expect(await Async.enumerate().take(5).toArray()).to.deepEqual([
          1,
          2,
          3,
          4,
          5
        ])
      })
    })

    yield* describe('skip', async function* () {
      yield* it('should skip n elements from the stream', async function* () {
        yield expect(await toArray(skip(enumerate(8), 3))).to.deepEqual([
          4,
          5,
          6,
          7,
          8
        ])
        yield expect(await Async.enumerate(8).skip(3).toArray()).to.deepEqual([
          4,
          5,
          6,
          7,
          8
        ])
      })

      yield* it('should handle an empty array', async function* () {
        yield expect(await toArray(skip(empty(), 3))).to.deepEqual([])
        yield expect(await Async.empty().skip(3).toArray()).to.deepEqual([])
      })

      yield* it('should return all elements if n is 0', async function* () {
        const array = [1, 2, 3, 4]
        yield expect(await toArray(skip(fromIterable(array), 0))).to.deepEqual(
          array
        )
        yield expect(
          await Async.fromIterable(array).skip(0).toArray()
        ).to.deepEqual(array)
      })
    })

    yield* describe('takeUntil', async function* () {
      yield* it('should take until the predicate is satisfied', async function* () {
        yield expect(
          await Async.enumerate(8)
            .takeUntil(e => e % 5 === 0)
            .toArray()
        ).to.deepEqual([1, 2, 3, 4])
      })

      yield* it('should handle an empty stream', async function* () {
        yield expect(
          await Async.empty()
            .takeUntil(e => e % 2)
            .toArray()
        ).to.deepEqual([])
      })

      yield* it('should return no values if the predicate is matched from the start', async function* () {
        yield expect(
          await Async.enumerate(3)
            .takeUntil(_ => true)
            .toArray()
        ).to.deepEqual([])
      })

      yield* it('should return all the values if the predicate is never matched', async function* () {
        const array = [1, 1, 2, 3, 5]
        yield expect(
          await Async.fromIterable(array)
            .takeUntil(_ => false)
            .toArray()
        ).to.deepEqual(array)
      })
    })

    yield* describe('skipUntil', async function* () {
      yield* it('should skip entries until the predicate is satisfied', async function* () {
        yield expect(
          await Async.enumerate(8)
            .skipUntil(e => e % 4 === 0)
            .toArray()
        ).to.deepEqual([4, 5, 6, 7, 8])
      })

      yield* it('should handle an empty stream', async function* () {
        expect(await Async.empty().skip(5).toArray()).to.deepEqual([])
      })

      yield* it('should return no values if the predicate is never matched', async function* () {
        yield expect(
          await Async.enumerate(8)
            .skipUntil(_ => false)
            .toArray()
        ).to.deepEqual([])
      })

      yield* it('should return all the values if the predicate is matched from the start', async function* () {
        const array = [1, 1, 2, 3, 5, 8]
        yield expect(
          await Async.fromIterable(array)
            .skipUntil(_ => true)
            .toArray()
        ).to.deepEqual(array)
      })
    })

    yield* describe('last', async function* () {
      yield* it('should return a stream only yielding the last value', async function* () {
        yield expect(await Async.enumerate(3).last().toArray()).to.deepEqual([
          3
        ])
      })
    })

    yield* describe('lastValue', async function* () {
      yield* it('should return the last value in the stream', async function* () {
        yield expect(await Async.enumerate(3).lastValue()).to.equal(3)
      })
    })

    yield* describe('merge', async function* () {
      yield* it('should merge multiple async streams', async function* () {
        yield expect(
          await Async.fromInterval(20)
            .map(i => `a${i}`)
            .merge(Async.fromInterval(40).map(i => `b${i}`))
            .take(8)
            .toArray()
        ).to.deepEqual(['a0', 'b0', 'a1', 'a2', 'b1', 'a3', 'a4', 'b2'])

        yield expect(
          await Async.fromInterval(20)
            .map(i => `a${i}`)
            .take(5)
            .merge(
              Async.fromInterval(40)
                .map(i => `b${i}`)
                .take(5)
            )
            .toArray()
        ).to.deepEqual([
          'a0',
          'b0',
          'a1',
          'a2',
          'b1',
          'a3',
          'a4',
          'b2',
          'b3',
          'b4'
        ])
      })
    })

    yield* describe('concat', async function* () {
      yield* it('should concatenate two streams', async function* () {
        const array1 = ['flurf', 'borf', 'bif'],
          array2 = ['smif', 'wurp', 'bawr']
        yield expect(
          await toArray(concat(fromIterable(array1), fromIterable(array2)))
        ).to.deepEqual(array1.concat(array2))
      })

      yield* it('should satisfy the associativity law', async function* () {
        const array1 = ['flurf', 'borf', 'bif'],
          array2 = ['smif', 'wurp', 'bawr'],
          array3 = ['weok', 'mwerf', 'pkaw']

        yield expect(
          await toArray(
            concat(
              concat(fromIterable(array1), fromIterable(array2)),
              fromIterable(array3)
            )
          )
        ).to.deepEqual(
          await toArray(
            concat(
              fromIterable(array1),
              concat(fromIterable(array2), fromIterable(array3))
            )
          )
        )
      })

      yield* it('should satisfy the identity law', async function* () {
        const array = ['qowk', 'yurp', 'larf']
        yield expect(
          await toArray(concat(fromIterable(array), empty()))
        ).to.deepEqual(array)
        yield expect(
          await toArray(concat(empty(), fromIterable(array)))
        ).to.deepEqual(array)

        yield expect(
          await Async.fromIterable(array).concat(Async.empty()).toArray()
        ).to.deepEqual(array)
        yield expect(
          await Async.empty().concat(Async.fromIterable(array)).toArray()
        ).to.deepEqual(array)
      })
    })

    yield* describe('zip', async function* () {
      yield* it('should zip streams', async function* () {
        yield expect(
          await toArray(zip(fromIterable(['foo', 'bar', 'baz']), enumerate(3)))
        ).to.deepEqual([
          ['foo', 1],
          ['bar', 2],
          ['baz', 3]
        ])

        yield expect(
          await Async.fromIterable(['foo', 'bar', 'baz'])
            .zip(Async.enumerate(3))
            .toArray()
        ).to.deepEqual([
          ['foo', 1],
          ['bar', 2],
          ['baz', 3]
        ])

        yield expect(
          await toArray(zip(enumerate(3), enumerate(5)))
        ).to.deepEqual([
          [1, 1],
          [2, 2],
          [3, 3],
          [null, 4],
          [null, 5]
        ])

        yield expect(
          await Async.enumerate(3).zip(Async.enumerate(5)).toArray()
        ).to.deepEqual([
          [1, 1],
          [2, 2],
          [3, 3],
          [null, 4],
          [null, 5]
        ])
      })

      yield* it('should handle empty streams', async function* () {
        yield expect(await toArray(zip(enumerate(3), empty()))).to.deepEqual([
          [1, null],
          [2, null],
          [3, null]
        ])

        yield expect(
          await Async.enumerate(3).zip(Async.empty()).toArray()
        ).to.deepEqual([
          [1, null],
          [2, null],
          [3, null]
        ])
      })
    })

    yield* describe('copy', async function* () {
      yield* it('should copy a stream', async function* () {
        const array = ['worf', 'borf', 'snarf']
        const stream = fromIterable(array)
        const [stream1, stream2] = copy(stream)

        yield expect(await toArray(stream1)).to.deepEqual(array)
        yield expect(await toArray(stream1)).to.deepEqual([])

        yield expect(await toArray(stream2)).to.deepEqual(array)
        yield expect(await toArray(stream2)).to.deepEqual([])

        const stream3 = Async.fromIterable(array)
        const [stream4] = await stream3.copy()

        yield expect(await stream3.toArray()).to.deepEqual(array)
        yield expect(await stream3.toArray()).to.deepEqual([])

        yield expect(await stream4.toArray()).to.deepEqual(array)
        yield expect(await stream4.toArray()).to.deepEqual([])
      })
    })

    yield* describe('intercalate', async function* () {
      yield* it('should intercalate a stream', async function* () {
        const array = ['morf', 'smif', 'tark']

        yield expect(
          await Async.fromIterable(array)
            .intercalate(' - ')
            .reduce((acc, e) => acc + e, '')
        ).to.equal('morf - smif - tark')
      })
    })

    yield* describe('some', async function* () {
      yield* it('should return early on a stream that has a matching value', async function* () {
        yield expect(await some(enumerate(), e => e === 7)).to.equal(true)
        yield expect(await Async.enumerate().some(e => e === 7)).to.equal(true)
      })

      yield* it('should work with empty streams', async function* () {
        expect(await some(empty(), _ => true)).to.equal(false)
        expect(await Async.empty().some(_ => true)).to.equal(false)
      })
    })

    yield* describe('every', async function* () {
      yield* it('should return early on a stream that has a non-matching value', async function* () {
        yield expect(await every(enumerate(), e => e < 10)).to.equal(false)
        yield expect(await Async.enumerate().every(e => e < 10)).to.equal(false)
      })

      yield* it('should work with empty streams', async function* () {
        yield expect(await every(empty(), _ => false)).to.equal(true)
        yield expect(await Async.empty().every(_ => false)).to.equal(true)
      })
    })

    yield* describe('automata', async function* () {
      yield* it('should transition states in an automata', async function* () {
        yield expect(
          await toArray(
            automata(fromIterable(['on', 'off', 'on']), 'disabled', {
              disabled: { on: 'enabled' },
              enabled: { off: 'disabled' }
            })
          )
        ).to.deepEqual(['disabled', 'enabled', 'disabled', 'enabled'])

        yield expect(
          await Async.fromIterable(['on', 'off'])
            .automata('disabled', {
              disabled: { on: 'enabled' },
              enabled: { off: 'disabled' }
            })
            .toArray()
        ).to.deepEqual(['disabled', 'enabled', 'disabled'])
      })
    })

    yield* describe('tap', async function* () {
      yield* it('should tap the values of the stream', async function* () {
        const original = console.log
        const log = mock()

        console.log = log
        const result = await toArray(tap(fromIterable(['foo', 'bar', 'baz'])))
        console.log = original

        yield expect(result).to.deepEqual(['foo', 'bar', 'baz'])
        yield expect(log.calls()).to.deepEqual([['foo'], ['bar'], ['baz']])

        console.log = log
        const result2 = await Async.fromIterable([
          'fiz',
          'buz',
          'borf'
        ]).tap.toArray()
        console.log = original

        yield expect(result2).to.deepEqual(['fiz', 'buz', 'borf'])
        yield expect(log.calls()).to.deepEqual([
          ['foo'],
          ['bar'],
          ['baz'],
          ['fiz'],
          ['buz'],
          ['borf']
        ])
      })
    })

    yield* describe('forEach', async function* () {
      yield* it('should iterate the values of the stream', async function* () {
        let values = []
        const fn = value => values.push(value)

        await forEach(fromIterable(['foo', 'bar', 'baz']), fn)
        yield expect(values).to.deepEqual(['foo', 'bar', 'baz'])

        values = []

        await Async.fromIterable(['fiz', 'buz', 'borf']).forEach(fn)
        yield expect(values).to.deepEqual(['fiz', 'buz', 'borf'])
      })
    })
  })
})

AsyncTest.tap(tests)
