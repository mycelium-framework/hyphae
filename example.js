let { Async } = await import('./index.modern.js')
Async.fromReadable(process.stdin).map(e => e.toString('utf8').toUpperCase()).toWritable(process.stdout)
