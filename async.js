const empty = async function* () {}

const enumerate = async function* (n) {
  n = n || Infinity
  for (let i = 1; i <= n; i++) yield i
}

const from = async function* (value) {
  yield value
}

const fromPromise = async function* (promise) {
  yield await promise
}

const fromIterable = async function* (iterable) {
  for await (let element of iterable) yield element
}

const fromReadableStream = readableStream => {
  let resolve = null,
    hasEnded = false,
    chunks = []

  let listener = chunk => {
    if (!hasEnded && !!resolve) {
      resolve({ done: false, value: chunk })
    } else if (hasEnded && !!resolve) {
      resolve({ done: true })
    } else {
      chunks.push(chunk)
    }
    resolve = null
  }

  readableStream.on('data', listener)

  readableStream.on('end', () => {
    hasEnded = true
    readableStream.off('data', listener)
    if (!!resolve) resolve({ done: true })
  })

  const next = async () => {
    const result = await new Promise(res => {
      const chunk = chunks.shift()
      if (chunk) res({ done: false, value: chunk })
      else if (!hasEnded) {
        resolve = res
      } else res({ done: true })
    })
    return result
  }

  return {
    next,
    [Symbol.asyncIterator]: () => ({
      next
    })
  }
}

const fromReadable = fromReadableStream

const fromEvent = (eventObj, eventName = null) => {
  let resolve = null,
    hasEnded = false,
    events = []

  const [on, off] = !!eventObj.addEventListener
    ? ['addEventListener', 'removeEventListener']
    : !!eventObj.addListener
    ? ['addListener', 'removeListener']
    : !!eventObj.on
    ? ['on', 'off']
    : []

  const listener = event => {
    if (!hasEnded && !!resolve) {
      resolve({ done: false, value: event })
    } else if (hasEnded && !!resolve) {
      resolve({ done: true })
    } else {
      events.push(event)
    }
    resolve = null
  }

  if (eventName !== null) eventObj[on](eventName, listener)
  else eventObj[on](listener)

  const unsubscribe = () => {
    if (eventName !== null) eventObj[off](eventName, listener)
    else eventObj[off](listener)

    if (!!resolve) {
      resolve({ done: true })
      resolve = null
    }
    hasEnded = true
  }

  const next = async () => {
    const result = await new Promise(res => {
      const event = events.shift()
      if (event) res({ done: false, value: event })
      else if (!hasEnded) {
        resolve = res
      } else res({ done: true })
    })
    return result
  }

  return {
    unsubscribe,
    next,
    [Symbol.asyncIterator]: () => ({
      next
    })
  }
}

const sleep = ms => new Promise(resolve => setTimeout(() => resolve(), ms))

const fromTimeout = async function* (value, ms) {
  await sleep(ms)
  yield value
}

const fromInterval = async function* (ms) {
  for (let i = 0; i < Infinity; i++) {
    await sleep(ms)
    yield i
  }
}

const toArray = async stream => {
  const array = []
  for await (let element of stream) array.push(element)
  return array
}

const toWritableStream = async (stream, writableStream) => {
  for await (let element of stream) writableStream.write(element)
  return writableStream
}

const toWritable = toWritableStream

const map = async function* (stream, f) {
  for await (let element of stream) yield f(element)
}

const filter = async function* (stream, p) {
  for await (let element of stream) if (p(element)) yield element
}

const scan = async function* (stream, f, initial) {
  let acc = initial
  for await (let element of stream) {
    acc = f(acc, element)
    yield acc
  }
}

const reduce = async (stream, f, initial) => {
  let acc = initial
  for await (let element of stream) {
    acc = f(acc, element)
  }
  return acc
}

const flatten = async function* (stream) {
  for await (let element of stream) yield* element
}

const take = async function* (stream, n) {
  for (let i = 0; i < n; i++) {
    const { done, value } = await stream.next()
    if (!done) yield value
    else return
  }
}

const skip = async function* (stream, n) {
  let done = false
  for (let i = 1; !done; i++) {
    const result = await stream.next()
    done = result.done
    if (i > n && !done) yield result.value
  }
}

const takeUntil = async function* (stream, p) {
  let done = false
  while (!done) {
    const result = await stream.next()
    done = p(result.value) || result.done
    if (!done) yield result.value
  }
}

const skipUntil = async function* (stream, p) {
  let done = false
  let matched = false
  while (!done) {
    const result = await stream.next()
    done = result.done
    matched = matched || p(result.value)
    if (matched && !done) yield result.value
  }
}

const last = async function* (stream) {
  let result
  for await (let element of stream) result = element
  yield result
}

const lastValue = async stream => reduce(stream, (_, e) => e, null)

const merge = async function* (...streams) {
  let streamPromises = []
  for (let i = 0; i < streams.length; i++) {
    streamPromises[i] = streams[i].next().then(result => ({ index: i, result }))
  }
  while (streamPromises.length) {
    const filteredPromises = streamPromises.filter(e => e)
    if (!filteredPromises.length) break

    const { index, result } = await Promise.race(filteredPromises)
    if (result.done) {
      streamPromises[index] = null
    } else {
      yield result.value
      streamPromises[index] = streams[index]
        .next()
        .then(result => ({ index, result }))
    }
  }
}

const concat = async function* (...streams) {
  for (let stream of streams) for await (let element of stream) yield element
}

const zip = async function* (...streams) {
  let done = false
  while (!done) {
    // TODO: use for loops
    const results = await Promise.all(
      streams.map(async stream => await stream.next())
    )
    done = results.every(result => result.done)
    if (!done) yield results.map(result => (result.done ? null : result.value))
  }
}

const copy = (stream, n = 2) => {
  let resolvedStream = [] // Resolve elements of the stream as they come
  let copyStreamPosition = []
  let copies = []
  for (let i = 0; i < n; i++) {
    copyStreamPosition[i] = 0
    copies.push(
      (async function* () {
        let isNotDone = true
        while (isNotDone) {
          const resolved =
            resolvedStream[copyStreamPosition[i]] || stream.next()
          resolvedStream[copyStreamPosition[i]] = resolved
          const { value, done } = await resolved

          isNotDone = !done
          if (isNotDone) yield value

          copyStreamPosition[i]++
        }
      })()
    )
  }
  return copies
}

const intercalate = async function* (stream, e) {
  let isFirst = true
  for await (let element of stream) {
    if (!isFirst) yield e
    yield element
    isFirst = false
  }
}

const some = async (stream, p) => {
  for await (let element of stream) if (p(element)) return true
  return false
}

const every = async (stream, p) => {
  for await (let element of stream) if (!p(element)) return false
  return true
}

const automata = async function* (stream, initial, machine) {
  let isValidInitial = false
  for (let [state, transitions] of Object.entries(machine)) {
    for (let [action, nextState] of Object.entries(transitions)) {
      if (typeof nextState !== 'string')
        throw new Error(
          `'${state}'->'${action}' transition has an invalid value`
        )
    }
    if (initial === state) isValidInitial = true
  }

  if (!isValidInitial) {
    throw new Error(`Initial value '${initial}' is not a valid state`)
  }

  yield initial

  let state = initial
  for await (let action of stream) {
    state = machine[state][action]
    if (state === undefined) {
      throw new Error(`'${state}'->'${action}' is not a valid transition`)
    }
    yield state
  }
}

const tap = async function* (stream) {
  for await (let element of stream) {
    console.log(element)
    yield element
  }
}

const forEach = async (stream, f) => {
  for await (let element of stream) f(element)
}

class Async {
  constructor(stream) {
    this.stream = stream
  }

  static empty() {
    return new Async(empty())
  }

  static enumerate(n) {
    return new Async(enumerate(n))
  }

  static from(value) {
    return new Async(from(value))
  }

  static fromPromise(promise) {
    return new Async(fromPromise(promise))
  }

  static fromIterable(iterable) {
    return new Async(fromIterable(iterable))
  }

  static fromEvent(eventObj, eventName) {
    const eventStream = new Async(fromEvent(eventObj, eventName))
    eventStream.unsubscribe = eventStream.stream.unsubscribe
    return eventStream
  }

  static fromReadableStream(readableStream) {
    return new Async(fromReadable(readableStream))
  }

  static fromReadable(readableStream) {
    return new Async(fromReadable(readableStream))
  }

  static fromTimeout(value, ms) {
    return new Async(fromTimeout(value, ms))
  }

  static fromInterval(ms) {
    return new Async(fromInterval(ms))
  }

  toArray() {
    return toArray(this.stream)
  }

  toWritable(writableStream) {
    return toWritable(this.stream, writableStream)
  }

  toWritableStream(writableStream) {
    return this.toWritable(this.stream, writableStream)
  }

  map(f) {
    return new this.constructor(map(this.stream, f))
  }

  filter(p) {
    return new this.constructor(filter(this.stream, p))
  }

  scan(f, initial) {
    return new this.constructor(scan(this.stream, f, initial))
  }

  reduce(f, initial) {
    return reduce(this.stream, f, initial)
  }

  static async *_flatten(stream) {
    for await (let element of stream) yield* element.stream
  }

  flatten() {
    return new this.constructor(Async._flatten(this.stream))
  }

  take(n) {
    return new this.constructor(take(this.stream, n))
  }

  skip(n) {
    return new this.constructor(skip(this.stream, n))
  }

  takeUntil(p) {
    return new this.constructor(takeUntil(this.stream, p))
  }

  skipUntil(p) {
    return new this.constructor(skipUntil(this.stream, p))
  }

  last() {
    return new this.constructor(last(this.stream))
  }

  lastValue() {
    return lastValue(this.stream)
  }

  merge(...streams) {
    const _streams = []
    for (let s of streams) _streams.push(s.stream)
    return new this.constructor(merge(this.stream, ..._streams))
  }

  concat(...streams) {
    const _streams = []
    for (let s of streams) _streams.push(s.stream)
    return new this.constructor(concat(this.stream, ..._streams))
  }

  zip(...streams) {
    const _streams = []
    for (let s of streams) _streams.push(s.stream)
    return new this.constructor(zip(this.stream, ..._streams))
  }

  async copy(n = 1) {
    const [original, ...copies] = await copy(this.stream, n + 1)
    this.stream = original

    const _copies = []
    for (let c of copies) _copies.push(new this.constructor(c))

    return _copies
  }

  intercalate(e) {
    return new this.constructor(intercalate(this.stream, e))
  }

  some(p) {
    return some(this.stream, p)
  }

  every(p) {
    return every(this.stream, p)
  }

  automata(initial, machine) {
    return new this.constructor(automata(this.stream, initial, machine))
  }

  get tap() {
    return new this.constructor(tap(this.stream))
  }

  async forEach(f) {
    await forEach(this.stream, f)
  }
}

export {
  Async,
  empty,
  enumerate,
  from,
  fromPromise,
  fromIterable,
  fromReadable,
  fromReadableStream,
  fromEvent,
  fromTimeout,
  fromInterval,
  toArray,
  toWritable,
  toWritableStream,
  map,
  filter,
  scan,
  reduce,
  flatten,
  take,
  skip,
  takeUntil,
  skipUntil,
  last,
  lastValue,
  merge,
  concat,
  zip,
  copy,
  intercalate,
  some,
  every,
  automata,
  tap,
  forEach
}
