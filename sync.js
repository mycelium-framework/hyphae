const empty = function* () {}

const enumerate = function* (n) {
  n = n || Infinity
  for (let i = 1; i <= n; i++) yield i
}

const from = function* (value) {
  yield value
}

const fromIterable = function* (array) {
  for (let element of array) yield element
}

const toArray = stream => [...stream]

const map = function* (stream, f) {
  for (let element of stream) yield f(element)
}

const filter = function* (stream, p) {
  for (let element of stream) if (p(element)) yield element
}

const scan = function* (stream, f, initial) {
  let acc = initial
  for (let element of stream) {
    acc = f(acc, element)
    yield acc
  }
}

const reduce = (stream, f, initial) => {
  let acc = initial
  for (let element of stream) {
    acc = f(acc, element)
  }
  return acc
}

const flatten = function* (stream) {
  for (let element of stream) yield* element
}

const take = function* (stream, n) {
  for (let i = 0; i < n; i++) {
    const { done, value } = stream.next()
    if (!done) yield value
    else return
  }
}

const skip = function* (stream, n) {
  let done = false
  for (let i = 1; !done; i++) {
    const result = stream.next()
    done = result.done
    if (i > n && !done) yield result.value
  }
}

const takeUntil = function* (stream, p) {
  let done = false
  while (!done) {
    const result = stream.next()
    done = p(result.value) || result.done
    if (!done) yield result.value
  }
}

const skipUntil = function* (stream, p) {
  let done = false
  let matched = false
  while (!done) {
    const result = stream.next()
    done = result.done
    matched = matched || p(result.value)
    if (matched && !done) yield result.value
  }
}

const last = function* (stream) {
  let result
  for (let element of stream) result = element
  yield result
}

const lastValue = stream => reduce(stream, (_, e) => e, null)

const concat = function* (...streams) {
  for (let stream of streams) for (let element of stream) yield element
}

const zip = function* (...streams) {
  let done = false
  while (!done) {
    const results = streams.map(stream => stream.next())
    done = results.every(result => result.done)
    if (!done) yield results.map(result => (result.done ? null : result.value))
  }
}

const copy = (stream, n = 2) => {
  let resolvedStream = [] // Resolve elements of the stream as they are pulled
  let copyStreamPosition = []
  let copies = []
  for (let i = 0; i < n; i++) {
    copyStreamPosition[i] = 0
    copies.push(
      (function* () {
        let isNotDone = true
        while (isNotDone) {
          const resolved =
            resolvedStream[copyStreamPosition[i]] || stream.next()
          resolvedStream[copyStreamPosition[i]] = resolved
          const { value, done } = resolved

          isNotDone = !done
          if (isNotDone) yield value

          copyStreamPosition[i]++
        }
      })()
    )
  }
  return copies
}

const intercalate = function* (stream, e) {
  let isFirst = true
  for (let element of stream) {
    if (!isFirst) yield e
    yield element
    isFirst = false
  }
}

const some = (stream, p) => {
  for (let element of stream) if (p(element)) return true
  return false
}

const every = (stream, p) => {
  for (let element of stream) if (!p(element)) return false
  return true
}

class Sync {
  constructor(stream) {
    this.stream = stream
  }

  static empty() {
    return new Sync(empty())
  }

  static enumerate(n) {
    return new Sync(enumerate(n))
  }

  static from(value) {
    return new Sync(from(value))
  }

  static fromPromise(promise) {
    return new Sync(fromPromise(promise))
  }

  static fromIterable(iterable) {
    return new Sync(fromIterable(iterable))
  }

  static fromEvent(eventName, eventObj, highWaterMark = Infinity) {
    return new Sync(fromEvent(eventName, eventObj, highWaterMark))
  }

  toArray() {
    return toArray(this.stream)
  }

  map(f) {
    return new this.constructor(map(this.stream, f))
  }

  filter(p) {
    return new this.constructor(filter(this.stream, p))
  }

  scan(f, initial) {
    return new this.constructor(scan(this.stream, f, initial))
  }

  reduce(f, initial) {
    return reduce(this.stream, f, initial)
  }

  static *_flatten(stream) {
    for (let element of stream) yield* element.stream
  }

  flatten() {
    return new this.constructor(Sync._flatten(this.stream))
  }

  take(n) {
    return new this.constructor(take(this.stream, n))
  }

  skip(n) {
    return new this.constructor(skip(this.stream, n))
  }

  takeUntil(p) {
    return new this.constructor(takeUntil(this.stream, p))
  }

  skipUntil(p) {
    return new this.constructor(skipUntil(this.stream, p))
  }

  last() {
    return new this.constructor(last(this.stream))
  }

  lastValue() {
    return lastValue(this.stream)
  }

  concat(...streams) {
    const _streams = []
    for (let s of streams) _streams.push(s.stream)
    return new this.constructor(concat(this.stream, ..._streams))
  }

  zip(...streams) {
    const _streams = []
    for (let s of streams) _streams.push(s.stream)
    return new this.constructor(zip(this.stream, ..._streams))
  }

  copy(n = 1) {
    const [original, ...copies] = copy(this.stream, n + 1)
    this.stream = original

    const _copies = []
    for (let c of copies) _copies.push(new this.constructor(c))

    return _copies
  }

  intercalate(e) {
    return new this.constructor(intercalate(this.stream, e))
  }

  some(p) {
    return some(this.stream, p)
  }

  every(p) {
    return every(this.stream, p)
  }
}

export {
  Sync,
  empty,
  enumerate,
  from,
  fromIterable,
  toArray,
  map,
  filter,
  scan,
  reduce,
  flatten,
  take,
  skip,
  takeUntil,
  skipUntil,
  last,
  lastValue,
  concat,
  zip,
  copy,
  intercalate,
  some,
  every
}
