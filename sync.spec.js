import {
  Sync,
  empty,
  enumerate,
  from,
  fromIterable,
  toArray,
  map,
  filter,
  scan,
  reduce,
  flatten,
  take,
  skip,
  takeUntil,
  skipUntil,
  concat,
  zip,
  copy,
  intercalate,
  some,
  every
} from './sync.js'

import {
  describe,
  xdescribe,
  it,
  xit,
  expect,
  Sync as SyncTest
} from 'polypore'

const tests = describe('Hyphae', function* () {
  yield* describe('Sync Stream', function* () {
    yield* describe('fromIterable', function* () {
      yield* it('should convert an array into a sync stream', function* () {
        const stream = fromIterable(['worf', 'borf', 'snarf'])
        yield expect(stream.next()).to.deepEqual({ done: false, value: 'worf' })
        yield expect(stream.next()).to.deepEqual({ done: false, value: 'borf' })
        yield expect(stream.next()).to.deepEqual({
          done: false,
          value: 'snarf'
        })
        yield expect(stream.next()).to.deepEqual({
          done: true,
          value: undefined
        })

        const streamObj = Sync.fromIterable(['worf', 'borf', 'snarf'])
        yield expect(streamObj.stream.next()).to.deepEqual({
          done: false,
          value: 'worf'
        })
        yield expect(streamObj.stream.next()).to.deepEqual({
          done: false,
          value: 'borf'
        })
        yield expect(streamObj.stream.next()).to.deepEqual({
          done: false,
          value: 'snarf'
        })
        yield expect(streamObj.stream.next()).to.deepEqual({
          done: true,
          value: undefined
        })
      })

      yield* it('should convert an empty array into a sync stream', function* () {
        const emptyStream = fromIterable([])
        yield expect(emptyStream.next()).to.deepEqual({
          done: true,
          value: undefined
        })

        const streamObj = Sync.fromIterable([])
        yield expect(streamObj.stream.next()).to.deepEqual({
          done: true,
          value: undefined
        })
      })
    })

    yield* describe('from', function* () {
      yield* it('should convert a value into a stream', function* () {
        yield expect(toArray(from('hello'))).to.deepEqual(['hello'])
        yield expect(Sync.from('hello').toArray()).to.deepEqual(['hello'])
      })
    })

    yield* describe('toArray', function* () {
      yield* it('should convert a sync stream into an array', function* () {
        const array = ['worf', 'borf', 'snarf']
        yield expect(toArray(fromIterable(array))).to.deepEqual(array)
        yield expect(Sync.fromIterable(array).toArray()).to.deepEqual(array)
      })

      yield* it('should convert an empty sync stream into an array', function* () {
        yield expect(toArray(empty())).to.deepEqual([])
        yield expect(Sync.empty().toArray()).to.deepEqual([])
      })
    })

    yield* describe('empty', function* () {
      yield* it('should return an empty stream', function* () {
        yield expect(toArray(empty())).to.deepEqual([])
        yield expect(Sync.empty().toArray()).to.deepEqual([])
      })
    })

    yield* describe('enumerate', function* () {
      yield* it('should enumerate n times', function* () {
        yield expect(toArray(enumerate(5))).to.deepEqual([1, 2, 3, 4, 5])
        yield expect(Sync.enumerate(5).toArray()).to.deepEqual([1, 2, 3, 4, 5])
      })
    })

    yield* describe('map', function* () {
      const fn = s => `{s}!`

      yield* it('should map over the stream', function* () {
        const array = ['mirf', 'truff', 'quif']
        yield expect(toArray(map(fromIterable(array), fn))).to.deepEqual(
          array.map(fn)
        )
        yield expect(Sync.fromIterable(array).map(fn).toArray()).to.deepEqual(
          array.map(fn)
        )
      })

      yield* it('should map over an empty stream', function* () {
        yield expect(toArray(map(empty(), fn))).to.deepEqual([])
        yield expect(Sync.empty().map(fn).toArray()).to.deepEqual([])
      })
    })

    yield* describe('filter', function* () {
      const even = e => e % 2 === 0

      yield* it('should filter the stream', function* () {
        yield expect(toArray(filter(enumerate(7), even))).to.deepEqual([
          2,
          4,
          6
        ])
        yield expect(Sync.enumerate(7).filter(even).toArray()).to.deepEqual([
          2,
          4,
          6
        ])
      })

      yield* it('should filter an empty stream', function* () {
        yield expect(toArray(filter(empty(), even))).to.deepEqual([])
        yield expect(Sync.empty().filter(even).toArray()).to.deepEqual([])
      })
    })

    yield* describe('transducer', function* () {
      yield* it('should be have like a transducer', function* () {
        let events = []

        Sync.enumerate(7)
          .map(e => {
            const result = e + 1
            events.push(`map: ${e} => ${result}`)
            return result
          })
          .filter(e => {
            const isFiltered = e % 2 === 0
            events.push(`filter: ${e} (${isFiltered})`)
            return isFiltered
          })
          .toArray()

        yield expect(events).to.deepEqual([
          'map: 1 => 2',
          'filter: 2 (true)',
          'map: 2 => 3',
          'filter: 3 (false)',
          'map: 3 => 4',
          'filter: 4 (true)',
          'map: 4 => 5',
          'filter: 5 (false)',
          'map: 5 => 6',
          'filter: 6 (true)',
          'map: 6 => 7',
          'filter: 7 (false)',
          'map: 7 => 8',
          'filter: 8 (true)'
        ])
      })
    })

    yield* describe('scan', function* () {
      const sum = (acc, e) => acc + e

      yield* it('should scan the stream', function* () {
        yield expect(toArray(scan(enumerate(5), sum, 0))).to.deepEqual([
          1,
          3,
          6,
          10,
          15
        ])
        yield expect(Sync.enumerate(5).scan(sum, 0).toArray()).to.deepEqual([
          1,
          3,
          6,
          10,
          15
        ])
      })

      yield* it('should scan the empty stream', function* () {
        yield expect(toArray(scan(empty(), sum, 0))).to.deepEqual([])
        yield expect(Sync.empty().scan(sum, 0).toArray()).to.deepEqual([])
      })
    })

    yield* describe('reduce', function* () {
      const sum = (acc, e) => acc + e

      yield* it('should reduce the stream', function* () {
        yield expect(reduce(enumerate(5), sum, 0)).to.equal(15)
        yield expect(Sync.enumerate(5).reduce(sum, 0)).to.equal(15)
      })

      yield* it('should reduce the empty stream', function* () {
        yield expect(reduce(empty(), sum, 0)).to.equal(0)
        yield expect(Sync.empty().reduce(sum, 0)).to.equal(0)
      })
    })

    yield* describe('flatten', function* () {
      yield* it('should flatten the stream', function* () {
        yield expect(
          Sync.fromIterable('hi')
            .map(char => Sync.fromIterable([10, 20, 30]).map(num => char + num))
            .flatten()
            .toArray()
        ).to.deepEqual(['h10', 'h20', 'h30', 'i10', 'i20', 'i30'])
      })
    })

    yield* describe('take', function* () {
      yield* it('should take n elements from the stream', function* () {
        yield expect(toArray(take(enumerate(8), 5))).to.deepEqual([
          1,
          2,
          3,
          4,
          5
        ])
        yield expect(Sync.enumerate(8).take(5).toArray()).to.deepEqual([
          1,
          2,
          3,
          4,
          5
        ])
      })

      yield* it('should handle an empty array', function* () {
        yield expect(toArray(take(empty(), 5))).to.deepEqual([])
        yield expect(Sync.empty().take(5).toArray()).to.deepEqual([])
      })

      yield* it('should handle an n greater than the number of elements in the stream', function* () {
        const array = [1, 2, 3, 4]
        yield expect(toArray(take(fromIterable(array), 5))).to.deepEqual(array)
        yield expect(Sync.fromIterable(array).take(5).toArray()).to.deepEqual(
          array
        )
      })

      yield* it('should limit an infinite series', function* () {
        yield expect(toArray(take(enumerate(), 5))).to.deepEqual([
          1,
          2,
          3,
          4,
          5
        ])
        yield expect(Sync.enumerate().take(5).toArray()).to.deepEqual([
          1,
          2,
          3,
          4,
          5
        ])
      })
    })

    yield* describe('skip', function* () {
      yield* it('should skip n elements from the stream', function* () {
        yield expect(toArray(skip(enumerate(8), 3))).to.deepEqual([
          4,
          5,
          6,
          7,
          8
        ])
        yield expect(Sync.enumerate(8).skip(3).toArray()).to.deepEqual([
          4,
          5,
          6,
          7,
          8
        ])
      })

      yield* it('should handle an empty array', function* () {
        yield expect(toArray(skip(empty(), 3))).to.deepEqual([])
        yield expect(Sync.empty().skip(3).toArray()).to.deepEqual([])
      })

      yield* it('should return all elements if n is 0', function* () {
        const array = [1, 2, 3, 4]
        yield expect(toArray(skip(fromIterable(array), 0))).to.deepEqual(array)
        yield expect(Sync.fromIterable(array).skip(0).toArray()).to.deepEqual(
          array
        )
      })
    })

    yield* describe('takeUntil', function* () {
      yield* it('should take until the predicate is satisfied', function* () {
        yield expect(
          Sync.enumerate(8)
            .takeUntil(e => e % 5 === 0)
            .toArray()
        ).to.deepEqual([1, 2, 3, 4])
      })

      yield* it('should handle an empty stream', function* () {
        yield expect(
          Sync.empty()
            .takeUntil(e => e % 2)
            .toArray()
        ).to.deepEqual([])
      })

      yield* it('should return no values if the predicate is matched from the start', function* () {
        yield expect(
          Sync.enumerate(3)
            .takeUntil(_ => true)
            .toArray()
        ).to.deepEqual([])
      })

      yield* it('should return all the values if the predicate is never matched', function* () {
        const array = [1, 1, 2, 3, 5, 8]
        yield expect(
          Sync.fromIterable(array)
            .skipUntil(_ => true)
            .toArray()
        ).to.deepEqual(array)
      })
    })

    yield* describe('skipUntil', function* () {
      yield* it('should skip entries until the predicate is satisfied', function* () {
        yield expect(
          Sync.enumerate(8)
            .skipUntil(e => e % 4 === 0)
            .toArray()
        ).to.deepEqual([4, 5, 6, 7, 8])
      })

      yield* it('should handle an empty stream', function* () {
        yield expect(Sync.empty().skip(5).toArray()).to.deepEqual([])
      })

      yield* it('should return no values if the predicate is never matched', function* () {
        yield expect(
          Sync.enumerate(8)
            .skipUntil(_ => false)
            .toArray()
        ).to.deepEqual([])
      })

      yield* it('should return all the values if the predicate is matched from the start', function* () {
        const array = [1, 1, 2, 3, 5, 8]
        yield expect(
          Sync.fromIterable(array)
            .skipUntil(_ => true)
            .toArray()
        ).to.deepEqual(array)
      })
    })

    yield* describe('last', function* () {
      yield* it('should return a stream only yielding the last value', function* () {
        yield expect(Sync.enumerate(3).last().toArray()).to.deepEqual([3])
      })
    })

    yield* describe('lastValue', function* () {
      yield* it('should return the last value in the stream', function* () {
        yield expect(Sync.enumerate(3).lastValue()).to.equal(3)
      })
    })

    yield* describe('concat', function* () {
      yield* it('should concatenate two streams', function* () {
        const array1 = ['flurf', 'borf', 'bif'],
          array2 = ['smif', 'wurp', 'bawr']
        yield expect(
          toArray(concat(fromIterable(array1), fromIterable(array2)))
        ).to.deepEqual(array1.concat(array2))
      })

      yield* it('should satisfy the associativity law', function* () {
        const array1 = ['flurf', 'borf', 'bif'],
          array2 = ['smif', 'wurp', 'bawr'],
          array3 = ['weok', 'mwerf', 'pkaw']

        yield expect(
          toArray(
            concat(
              concat(fromIterable(array1), fromIterable(array2)),
              fromIterable(array3)
            )
          )
        ).to.deepEqual(
          toArray(
            concat(
              fromIterable(array1),
              concat(fromIterable(array2), fromIterable(array3))
            )
          )
        )
      })

      yield* it('should satisfy the identity law', function* () {
        const array = ['qowk', 'yurp', 'larf']
        yield expect(
          toArray(concat(fromIterable(array), empty()))
        ).to.deepEqual(array)
        yield expect(
          toArray(concat(empty(), fromIterable(array)))
        ).to.deepEqual(array)

        yield expect(
          Sync.fromIterable(array).concat(Sync.empty()).toArray()
        ).to.deepEqual(array)
        yield expect(
          Sync.empty().concat(Sync.fromIterable(array)).toArray()
        ).to.deepEqual(array)
      })
    })

    yield* describe('zip', function* () {
      yield* it('should zip streams', function* () {
        yield expect(
          toArray(zip(fromIterable(['foo', 'bar', 'baz']), enumerate(3)))
        ).to.deepEqual([
          ['foo', 1],
          ['bar', 2],
          ['baz', 3]
        ])

        yield expect(
          Sync.fromIterable(['foo', 'bar', 'baz'])
            .zip(Sync.enumerate(3))
            .toArray()
        ).to.deepEqual([
          ['foo', 1],
          ['bar', 2],
          ['baz', 3]
        ])

        yield expect(toArray(zip(enumerate(3), enumerate(5)))).to.deepEqual([
          [1, 1],
          [2, 2],
          [3, 3],
          [null, 4],
          [null, 5]
        ])

        yield expect(
          Sync.enumerate(3).zip(Sync.enumerate(5)).toArray()
        ).to.deepEqual([
          [1, 1],
          [2, 2],
          [3, 3],
          [null, 4],
          [null, 5]
        ])
      })

      yield* it('should handle empty streams', function* () {
        yield expect(toArray(zip(enumerate(3), empty()))).to.deepEqual([
          [1, null],
          [2, null],
          [3, null]
        ])

        yield expect(
          Sync.enumerate(3).zip(Sync.empty()).toArray()
        ).to.deepEqual([
          [1, null],
          [2, null],
          [3, null]
        ])
      })
    })

    yield* describe('copy', function* () {
      yield* it('should copy a stream', function* () {
        const array = ['worf', 'borf', 'snarf']
        const [stream1, stream2] = copy(fromIterable(array))

        yield expect(toArray(stream1)).to.deepEqual(array)
        yield expect(toArray(stream1)).to.deepEqual([])

        yield expect(toArray(stream2)).to.deepEqual(array)
        yield expect(toArray(stream2)).to.deepEqual([])

        const stream3 = Sync.fromIterable(array)
        const [stream4] = stream3.copy()

        yield expect(stream3.toArray()).to.deepEqual(array)
        yield expect(stream3.toArray()).to.deepEqual([])

        yield expect(stream4.toArray()).to.deepEqual(array)
        yield expect(stream4.toArray()).to.deepEqual([])
      })
    })

    yield* describe('intercalate', function* () {
      yield* it('should intercalate a stream', function* () {
        const array = ['morf', 'smif', 'tark']

        yield expect(
          Sync.fromIterable(array)
            .intercalate(' - ')
            .reduce((acc, e) => acc + e, '')
        ).to.equal('morf - smif - tark')
      })
    })

    yield* describe('some', function* () {
      yield* it('should return early on a stream that has a matching value', function* () {
        yield expect(some(enumerate(), e => e === 7)).to.equal(true)
        yield expect(Sync.enumerate().some(e => e === 7)).to.equal(true)
      })

      yield* it('should work with empty streams', function* () {
        yield expect(some(empty(), _ => true)).to.equal(false)
        yield expect(Sync.empty().some(_ => true)).to.equal(false)
      })
    })

    yield* describe('every', function* () {
      yield* it('should return early on a stream that has a non-matching value', function* () {
        yield expect(every(enumerate(), e => e < 10)).to.equal(false)
        yield expect(Sync.enumerate().every(e => e < 10)).to.equal(false)
      })

      yield* it('should work with empty streams', function* () {
        yield expect(every(empty(), _ => false)).to.equal(true)
        yield expect(Sync.empty().every(_ => false)).to.equal(true)
      })
    })
  })
})

SyncTest.tap(tests)
